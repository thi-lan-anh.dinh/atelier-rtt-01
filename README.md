# Atelier RTT 01
Atelier d'introduction à R et ggplot2 du 13/04/2023

### Lien
https://ecosys-rtt.pages.mia.inra.fr/atelier-rtt-01
(ne fonctionne pas pour le moment)

### Organisation des fichiers
Exemple d'organisation de projet pour workflow reproductible

- `data/` Fichiers de données (brutes et après traitement)
    -  `data-raw`
    - `data-processed`
- `R` Scripts R
    - `01_data_cleanup.R` Traitement des donées et export vers data-clean
    - `02_data_analysis.R` Analyse des données
- `outputs/` Sorties des scripts R
    - `mods` sauvegarde des modèles statistiques
- `public` Rapport Quarto (en phase de test)



