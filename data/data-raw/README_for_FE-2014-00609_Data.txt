Dataset description for:
FE-2014-00609
Under the influence: sublethal exposure to an insecticide affects personality expression in a jumping spiderRaphaël Royauté, Christopher M. Buddle & Charles Vincent(Functional Ecology 2015)Corresponding author: Raphaël Royauté (raphael.royaute@gmail.com)


Dataset Description: Behavioural data for insecticide-exposed and control groups.

Abbreviations: _log indicates natural log-transformed data, _sqrt indicates square-root transformed data.

Column description:

General information columns:
Id: Indicates the Id code for each individual.
Test: Indicates whether the behavioral test were conducted before or after the 24h insecticide exposure phase.
Treatment: Indicates the experimental treatment received by the individual (exposed to the insecticide or not exposed)
Batch: Indicates the experimental batch each individual belonged to
Date: Indicates the date at which the behavioral test were performed
Population: Indicates the population where the individual was collected. ARBO: McGill Morgan Arboretum (Ste Anne de Bellevue, QC, W 45.440185, N -73.946893), ORCH: Insecticide-free apple orchard (Agriculture and Agri-Food Canada experimental farm in Frelighsburg, QC, W 45.0462, N -72.8565), LAB: Laboratory-reared population, STC: Pin Rigide Ecological Reserve (Saint-Chrysostome, QC, W 45.111657, N -73.876557).
Rearing: Wether the individual was collected in the field (Field) or reared in the laboratory (Lab).
Sex: The sex of the individual (F: female, M: male).
Size: The body-size of the spider in mm.
Mass: The body-mass of the spider in mg.
Condition: The body condition of the spider taken as the residuals of the linear regression of body-mass over body-size. This index indicates whether the spider is well-fed (positive value, mass heavier than body-size predicts) or starved (negative value, mass lighter than predicted by body-size).

Open-field exploration test columns: Each spider was introduced in an open-field arena subdivided into 36 5 x 5 cm quadrats. The spider was introduced at the center of the arena  and was allowed to move freely into the arena for 300 s. 
Total_Activity: The total number of quadrats travelled by the spider during the open-field exploration test.
Surface_Explored: The number of unique quadrats explored by the spider during the open-field exploration test.
Central_Activity: The number of quadrats visited in the central zone of the arena during the open-field exploration test.
Intermediate_Activity: The number of quadrats visited in the intermediate zone of the arena during the open-field exploration test.
Edge_Activity: The number of quadrats visited in the edge zone of the arena during the open-field exploration test.
First_Move_Lat: The latency to exit the first quadrat after the spider was inserted into the arena (in seconds).
First_Minute_Activity: The number of quadrants visited during the first minute of the open-field exploration test.

Prey-capture test columns: Each spider was introduced in a 9 cm Petri dish containing a prey item and was left a maximum of 600s to capture the prey.
Droso: The drosophila species given to the spider during the prey capture test (hydei: D. hydei, melano: D. melanogaster)
Capture: Whether the spider succeeded (1) or failed (0) in capturing the prey item.
Lat_Detect: Time before the first noticeable orientation of the spider toward the prey item (in seconds).
Lat_Capt: Time before capture of the prey item (in seconds). Spiders that failed to capture the prey were assigned a value of 600s for capture latency.
Attacks: The number of attack attempts on the prey item.
Active_Tracking_mean: The mean duration of active tracking bouts, defined as a fast forward motion toward the prey, often concluded by a capture attempt (in seconds). 
Visual_Tracking_mean: The mean duration of visual tracking bouts, defined as instances where the spider oriented its cephalothorax toward the prey and visually followed the prey at a distance (in seconds).