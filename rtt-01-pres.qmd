---
title: "Ateliers R Trucs et Astuces"
subtitle: "Introduction à R et représentation graphiques sous `ggplot2`"
format:
  revealjs: 
    logo: images/RTT.png
    theme: night
    incremental: false
    preview-links: true
    code-fold: false
    smaller: true
    footnotes-hover: true
    link-external-newwindow: true
editor: source
  # markdown: 
    # wrap: 72
author: Raphaël Royauté, Lucie Martin, Pierre-Antoine Précigout, Sébastien Saint-Jean 
institute: INRAE UMR EcoSys
date: 13-04-2023
date-format: short
#bibliography: references.bib
---


## 
| Organisation |
|----------------------|
| Utilisation générale |
| Créer son projet |
| Importer des données |
| Graphiques avec `ggplot2` et ``palmerpenguins` |

## Utilisation générale

### Packages nécessaires
- `tidyverse`
- `palmerpenguins`


```{r}
#| eval: false
#| echo: true
install.packages(c("tidyverse", "palmerpenguins")) # télécharger les packages
library(tidyverse); library(palmerpenguins) # Importer les packages

```


## Utilisation générale

### Le langage R en (très) bref
- Déclaration de variables

```{r}
#| echo: true
a <- 10
b <- 18
a / b
```
## Utilisation générale

### Le langage R en (très) bref
- Vecteurs et matrices
```{r}
#| echo: true
Vecteur <- c(1, 2, 3)
Matrice <- matrix(c(1, 2, 3, 4, 5, 6, 7, 8, 9), 
                  byrow = T, nrow = 3, ncol = 3)
Vecteur; Matrice
```

## Utilisation générale
### Le langage R en (très) bref
- Boucles
```{r}
#| echo: true
#| code-fold: true
n <- 10
y <- c() # vecteur vide
x <- c() # vecteur vide
for (i in 1:n) { # Pour i allant de 1 à n
  x[i] <- runif(1, 1, 5) # Tirer un nombre entre 1 et 5
  y[i] <- a * x[i] + b # Calculer y à chaque itération i
}
plot(x,y) # Visualiser
```

## Utilisation générale
### Le langage R en (très) bref
- Fonctions

```{r}
#| echo: true
#| code-fold: true
# Générer des données simulées
fonction_data <- function(n1, n2, m1, m2, s1, s2) {
  # n1: taille d'échantillon contôle
  # n2: taille d'échantillon traité
  # m1: moyenne échantillon contôle
  # m2: moyenne échantillon traité
  # s1: écart type échantillon contôle
  # s2: écart type échantillon traité
  
  ctrl <- rnorm(n1, m1, s1) # Simuler échantillon contôle
  trt <- rnorm(n2, m2, s2) # Simuler échantillon traité
  df <- data.frame(Trt = c(rep("Ctrl", n1), 
                           rep("Trt", n2)),
                   Activity = c(ctrl, trt)) # Stocker les données
  return(df)
}
dsim <- fonction_data(50, 50, 50, 30, 5, 3) # Appeler la fonction
hist(dsim$Activity) # Visualiser
```



## Utilisation générale

### Le langage R en (très) bref
- Dataframes
  - Classe d'objet très fréquemment utilisée
  - Permet de combiner différentes classes de données
  - Numérique, caractères, facteurs
  - Permet d'importer des données externes dans R 

```{r}
#| echo: true
str(dsim)
```



## Créer son projet
- File -> New Project
- Outil de gestion de projet très puissant et intuitif
- Permet organisation des fichiers, scripts et sorties
- Chemins d'accès relatifs

![](images/ProjectArchitecture_1.png)

## Importer des données
- Créer fichiers data/data-raw
- Téléchargez les feuilles de calcul [ici](https://forgemia.inra.fr/ecosys-rtt/atelier-rtt-01/-/tree/main/data/data-raw)
- Donnée de [tests comportementaux sur araignées sauteuses](https://besjournals.onlinelibrary.wiley.com/doi/full/10.1111/1365-2435.12413)
- Import depuis RStudio
  - Option "Import Dataset"
- Import par fonctions `read.csv()` ou `read.table()`

```{r}
#| eval: false
#| echo: true
df <- read.csv(file = "mon_fichier.csv", 
               header = T, # Conserver le nom des variables
               sep = ";", # Séparateur de colonnes
               dec = ",") # Décimales '.' ou ',' dans feuille de calcul
```


## Exercice 1: Manipuler les données
- Importez les fichiers `data_virg` & `data_tab`
- Séléctionnez la colonne `Treatment` avec df$
- Séléctionnez les mâles de la phase `post-treatment` avec df[,]
- Séléctionnez les mâles de la population `post-treatment` avec `filter`
- Créez une nouvelle colonne log(Activity)
- Comparez l'activité mâle vs. femelles en phase post-traitement avec `boxplot()`

## Exporter des données transformées
- commande `write.cdv()`
- indiquer chemin d'accès pour sauvegarder `path = "data/data-clean/data_clean.csv"`

```{r}
#| eval: false
write.csv(df, "data/data-clean/data_clean.csv")
```


## Graphiques avec `ggplot2` et `palmerpenguins`

![](images/palmerpenguins.png){.absolute left="0" bottom="0" width="300"} ![](images/README-flipper-bill-1.png){.absolute right="0" bottom="0" width="350"}








## Anatomie d'un `ggplot`
### Elements principaux

| Elément/Fonction | Explication |
|------------------|:------------|
| `ggplot()` | Fonction principale |
| `aes()` | aesthetics: contrôle les éléments du graphique (x, y, colour, fill, shape, size)|
| `geom_()` | Contrôle les éléments graphiques (points, boxplot, density, ... )|
| `theme()` | Customization de l'apparence du graphique|








## Anatomie d'un `ggplot`
### Graphique simple

```{r}
#| warning: false
#| echo: true
library(palmerpenguins); library(tidyverse)

ggplot(penguins, 
       aes(y = body_mass_g, x = species)) +
  geom_boxplot() 
```

## Anatomie d'un `ggplot`
### Customisation

```{r}
#| warning: false
#| echo: true
#| code-fold: true
library(palmerpenguins); library(tidyverse)

ggplot(penguins, 
       aes(y = body_mass_g, x = species)) +
  geom_boxplot(aes(fill = species)) +
  geom_jitter(alpha = .6, size = 2.5, width = .2, color = "black") +
  facet_wrap(~sex) +
  labs(y = "Masse (g)", x = "Espèce") +
  theme_bw(14)
```



## Exercice 2 : Graphique complexe
- Représentez la taille des ailes en fonction de la masse par espèces
- Ailes = "flipper"
- Subdivisez par le sexe des individus
- Retirer les individus de sexe inconnu

::: {.fragment}
::: {.callout-tip}
## Astuces
- Google est votre ami !
- Est-ce qu'il existe pas une fonction qui retire les valeures `NA` ?
:::
:::


## Exercice 2 : Graphique complexe
### Solution

```{r}
#| warning: false
#| echo: true
#| code-fold: true
penguins %>% 
  drop_na(sex) %>% 
  ggplot(aes(y = body_mass_g, x = flipper_length_mm, color = species)) +
  geom_point(alpha = .6, size = 2.5) +
  facet_wrap(~sex) +
  labs(y = "Masse (g)", x = "Ailes (mm)") +
  theme_bw(14)
```


## Exercice 3 : Lâchez-vous !
- Reproduisez un style de graphique sur [R Graphical Gallery](https://r-graph-gallery.com/)
- [`ggplot2` cheatsheet](https://thinkr.fr/pdf/ggplot2-french-cheatsheet.pdf)


## Resources

:::: {.columns}

::: {.column width="50%"}
Tutoriels
  - [Data Science avec R](https://bookdown.org/fousseynoubah/dswr_book/)
  - [R Cookbook](https://rc2e.com/)
  - [Quick-R](https://www.statmethods.net/)

Galleries
  - [R Graphics Gallery](https://r-graph-gallery.com/)

Livres
  - [The R Book](https://www.wiley.com/en-us/The+R+Book%2C+2nd+Edition-p-9780470973929)
  - [Statistical Rethinking](https://xcelab.net/rm/statistical-rethinking/)
:::

::: {.column width="50%"}
Cours
  - [Lectures Statistical Rethinking](https://www.youtube.com/playlist?list=PLDcUM9US4XdMROZ57-OIRtIK0aOynbgZN)
  - [eLearning INRAE](https://elearning.formation-permanente.inrae.fr/course/view.php?id=567#)

Débuggage
  - [Stack Overflow](https://stackoverflow.com/)
  - [R-help](https://stat.ethz.ch/mailman/listinfo/r-help)

:::

::::



